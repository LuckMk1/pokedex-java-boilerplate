package com.nmeo.models;

import java.util.HashSet;

public class Pokemon {

    private String pokemonName;

    private PokemonType type;

    private int lifePoints;

    private HashSet<PokemonPower> powers;

    public Pokemon(){}

    public Pokemon(String pokemonName, PokemonType type, int lifePoints, HashSet<PokemonPower> powers) {
        this.pokemonName = pokemonName;
        this.type = type;
        this.lifePoints = lifePoints;
        this.powers = powers;
    }

    public String getPokemonName() {
        return pokemonName;
    }

    public PokemonType getType() {
        return type;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public HashSet<PokemonPower> getPowers() {
        return powers;
    }

    public void setType(PokemonType type) {
        this.type = type;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public void setPowers(HashSet<PokemonPower> powers) {
        this.powers = powers;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pokemon) {
            Pokemon pokemon = (Pokemon) obj;
            return pokemon.getPokemonName().equals(this.pokemonName);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "pokemonName='" + pokemonName + '\'' +
                ", type=" + type +
                ", lifePoints=" + lifePoints +
                ", powers=" + powers.toString() +
                '}';
    }

    @Override
    public int hashCode() {
        return this.pokemonName.hashCode();
    }
}
