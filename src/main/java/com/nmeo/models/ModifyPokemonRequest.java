package com.nmeo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashSet;

public class ModifyPokemonRequest {

    private String pokemonName;

    @JsonIgnoreProperties
    private PokemonType type;
    @JsonIgnoreProperties
    private int lifePoints;
    @JsonIgnoreProperties
    private HashSet<PokemonPower> powers;

    public ModifyPokemonRequest() {
    }

    public String getPokemonName() {
        return pokemonName;
    }

    public void setPokemonName(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    public PokemonType getType() {
        return type;
    }

    public void setType(PokemonType type) {
        this.type = type;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public HashSet<PokemonPower> getPowers() {
        return powers;
    }

    public void setPowers(HashSet<PokemonPower> powers) {
        this.powers = powers;
    }
}
