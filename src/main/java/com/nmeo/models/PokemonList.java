package com.nmeo.models;

import java.util.HashSet;

public class PokemonList {

    private HashSet<Pokemon> result;

    public PokemonList() {}

    public PokemonList(HashSet<Pokemon> pokemonlist) {
        this.result = pokemonlist;
    }

    public void add(Pokemon pokemon) {
        this.result.add(pokemon);
    }

    public int size() {
        return this.result.size();
    }

    public HashSet<Pokemon> getResult() {
        return result;
    }
}
