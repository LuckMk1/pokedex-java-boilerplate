package com.nmeo.models;

public class PokemonPower {

    private String powerName;

    private PokemonType damageType;

    private int damage;

    public PokemonPower(){}

    public PokemonPower(String powerName, PokemonType damageType, int damage) {
        this.powerName = powerName;
        this.damageType = damageType;
        this.damage = damage;
    }

    public String getPowerName() {
        return powerName;
    }

    public PokemonType getDamageType() {
        return damageType;
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public String toString(){
        return "Power: " + this.powerName + " Damage: " + this.damage + " Type: " + this.damageType;
    }

    @Override
    public int hashCode() {
        return this.powerName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PokemonPower) {
            PokemonPower pokemonPower = (PokemonPower) obj;
            System.out.println(pokemonPower.getPowerName().equals(this.powerName));
            return pokemonPower.getPowerName().equals(this.powerName);
        }
        return false;
    }
}
