package com.nmeo;

import com.nmeo.models.*;
import io.javalin.Javalin;

import org.apache.commons.lang3.EnumUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.HashSet;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());

    private static final HashSet<Pokemon> pokemonList = new HashSet<>();

    private static final Object mutex = new Object();

    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null ? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        Javalin.create()
                .get("/api/status", ctx -> {
                    logger.debug("Status handler triggered", ctx);
                    ctx.status(200);
                })
                .post("/api/create", ctx -> {
                    synchronized (mutex) {
                        try {
                            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
                            if (pokemonList.contains(pokemon)) {
                                ctx.status(400);
                                return;
                            }
                            pokemonList.add(pokemon);
                            ctx.status(200);
                        } catch (Exception e) {
                            ctx.json("Invalid pokemon data:" + e.getMessage());
                            ctx.status(400);
                        }
                    }
                })
                .get("/api/searchByName", ctx -> {
                    try {
                        String nameToSearch = ctx.queryParam("name");
                        if (nameToSearch == null || nameToSearch.isEmpty()) {
                            ctx.json(new HashSet<>());
                            ctx.status(200);
                            return;
                        }
                        PokemonList results = new PokemonList(new HashSet<>());
                        for (Pokemon pokemon : pokemonList) {
                            if (pokemon.getPokemonName().contains(nameToSearch)) {
                                results.add(pokemon);
                            }
                        }
                        if (results.size() > 0) {
                            ctx.json(results);
                            ctx.status(200);
                        } else {
                            ctx.json(new HashSet<>());
                            ctx.status(200);
                        }
                    } catch (Exception e) {
                        ctx.json("Invalid name:" + e.getMessage());
                        ctx.status(400);
                    }
                })
                .get("/api/searchByType", ctx -> {
                    try {
                        String typeToSearch = ctx.queryParam("type");
                        if (typeToSearch == null || typeToSearch.isEmpty() || !EnumUtils.isValidEnum(PokemonType.class, typeToSearch)) {
                            ctx.json(new HashSet<>());
                            ctx.status(400);
                            return;
                        }
                        PokemonList results = new PokemonList(new HashSet<>());
                        for (Pokemon pokemon : pokemonList) {
                            if (pokemon.getType().toString().equals(typeToSearch)) {
                                results.add(pokemon);
                            }
                        }
                        if (results.size() > 0) {
                            ctx.json(results);
                            ctx.status(200);
                        } else {
                            ctx.json(new HashSet<>());
                            ctx.status(200);
                        }
                    } catch (Exception e) {
                        ctx.json("Invalid type:" + e.getMessage());
                        ctx.status(400);
                    }
                })
                .post("/api/modify", ctx -> {
                    synchronized (mutex) {
                        try {
                            ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);
                            for (Pokemon pokemon : pokemonList) {
                                if (pokemon.getPokemonName().equals(request.getPokemonName())) {
                                    if (request.getLifePoints() > 0) pokemon.setLifePoints(request.getLifePoints());
                                    if (request.getType() != null) pokemon.setType(request.getType());
                                    if (request.getPowers() != null) {
                                        for (PokemonPower power: request.getPowers()) {
                                            pokemon.getPowers().remove(power);
                                            pokemon.getPowers().add(power);
                                        }
                                    }
                                    ctx.status(200);
                                    return;
                                }
                                ctx.status(404);
                            }
                        } catch (Exception e) {
                            ctx.json("Invalid pokemon data:" + e.getMessage());
                            ctx.status(400);
                        }
                    }
                })
                .start(port);
    }
}